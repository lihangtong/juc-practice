package com.jdkman.thread.sync;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 可见性。number被修改后，子线程无法看到，子线程读取的是自己工作内存中的副本
 */
@Slf4j
public class NoVisibility {

    private static boolean ready;

    private static int number;

    private static class ReaderThread extends Thread {
        @Override
        public void run() {
            while(true) {
                if (number > 0) {
                    log.info("number = {}", number);
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        new ReaderThread().start();

        SleepUtils.sleep(1000);

        number = 42;

        ready = true;

        log.info("main thread end.");
    }

}
