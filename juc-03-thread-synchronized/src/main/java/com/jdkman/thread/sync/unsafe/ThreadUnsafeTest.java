package com.jdkman.thread.sync.unsafe;

import com.jdkman.thread.sync.model.MyLongAddr;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadUnsafeTest {

    /**
     * 非线程安全，共享变量的修改，没有加锁
     * @param args
     */
    public static void main(String[] args) {
        MyLongAddr longAddr = new MyLongAddr();

        for (int i = 0; i < 10000; i++) {
            Thread thread = new Thread(() -> longAddr.incr());
            thread.start();
        }

        log.info("value = {}", longAddr.getValue());
    }

}
