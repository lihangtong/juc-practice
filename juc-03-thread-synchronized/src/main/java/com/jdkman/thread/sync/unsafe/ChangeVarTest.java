package com.jdkman.thread.sync.unsafe;

import lombok.extern.slf4j.Slf4j;

/**
 * 描述：两个线程同时修改一个共享变量，存在并发修改问题
 * 最后的结果，不一定等于9
 *
 * 在java的内存模型中，要完成静变量的自增，需要在主内存和工作内存中进行变量的交换。
 * Java中，对金泰变量的自增、自建，不是原子操作。需要4个字节码指令的参与。
 */
@Slf4j
public class ChangeVarTest {

    private static int num = 0;

    public static void main(String[] args) throws InterruptedException {
        test();
    }

    private static void test() throws InterruptedException {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 5000; i++) {
                num++;
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 5000; i++) {
                num--;
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        log.info("num is :{}", num);
        log.info("main thread end");
    }


}
