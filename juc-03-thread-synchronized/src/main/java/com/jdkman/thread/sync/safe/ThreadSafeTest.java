package com.jdkman.thread.sync.safe;

import com.jdkman.thread.sync.model.MyLongAddrSync;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadSafeTest {

    private static int num;

    public static void main(String[] args) {

        syncTest01();

    }

    /**
     * 使用synchronized加锁，保证共享变量的安全
     *  1、支持普通方法
     *  2、支持静态方案
     *  3、支持代码块加锁
     */
    private static void syncTest01() {
        MyLongAddrSync sync = new MyLongAddrSync();

        for (int i = 0; i < 10000; i++) {
            Thread thread = new Thread(() -> sync.incr());

            thread.start();
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        log.info("main thread finish:{}", sync.getValue());

    }


}
