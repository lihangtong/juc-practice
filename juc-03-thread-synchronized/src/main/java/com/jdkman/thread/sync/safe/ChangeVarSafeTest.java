package com.jdkman.thread.sync.safe;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ChangeVarSafeTest {

    private static int num = 0;

    // 采购对象锁的方式，来控制并发修改
    private static Object monitor = new Object();

    public static void main(String[] args) throws InterruptedException {
        test();
    }

    private static void test() throws InterruptedException {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 5000; i++) {
                synchronized (monitor) {
                    num++;
                }
            }
        });


        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 5000; i++) {
                synchronized (monitor) {
                    num--;
                }
            }
        });


        t1.start();
        t2.start();

        t1.join();
        t2.join();


        log.info("num always is: {}", num);

        log.info("main thread end");
    }

}
