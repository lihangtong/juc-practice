package com.jdkman.thread.sync.model;

public class MyLongAddrSync {

    private int value;

    /**
     * 本次，采用sync锁的方式，普通方法上加锁
     */
    public synchronized void incr() {
        value++;
    }

    public int getValue() {
        return value;
    }

}
