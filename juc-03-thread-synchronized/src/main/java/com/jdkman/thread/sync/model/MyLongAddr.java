package com.jdkman.thread.sync.model;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lihangtong@outlook.com
 */
@Slf4j
public class MyLongAddr {

    private int value;

    /**
     * 做加法
     */
    public void incr() {
        value++;
        log.info("value changed to : {}", value);
    }

    /**
     * 做减法
     */
    public void decr() {
        value--;
    }

    public int getValue() {
        return value;
    }
}
