package com.jdkman.thread.state;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;


/**
 * 线程分两种
 *  1、用户线程，所有的用户线程退出了，jvm才会退出
 *  2、守护线程
 */
@Slf4j
public class DaemonThreadTest {

    public static void main(String[] args) {
        testUser();
//        testDaemon();
    }

    /**
     * 测试用户线程
     */
    private static void testUser() {
        Thread thread = new Thread(() -> {
            log.info("son thread begin");

            SleepUtils.sleep(5000);

            log.info("son thread end");
        });

        thread.start();

        log.info("main thread end");
    }

    private static void testDaemon() {
        Thread thread = new Thread(() -> {
            log.info("son thread begin");

            SleepUtils.sleep(5000);

            log.info("son thread end");
        });

        thread.setDaemon(true);

        thread.start();

        log.info("main thread end");
    }

}
