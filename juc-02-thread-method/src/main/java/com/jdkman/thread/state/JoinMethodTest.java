package com.jdkman.thread.state;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 1、当没有用join的时候
 */
@Slf4j
public class JoinMethodTest {

    private static int num = 0;

    public static void main(String[] args) {
        test();
    }

    /**
     * 主线程和子线程 是并行执行的
     * 而主线程要打印r的正确结果，必须等子线程运行结束，此时，可以使用join方法
     *
     * join() 等待线程运行结束
     * join(long n) 等待线程运行结束，最多等待n毫秒
     */
    private static void test() {
        Thread thread = new Thread(() -> {
            log.info("thread begin");

            SleepUtils.sleep(5000);

            num = 10;
            log.info("thread end");
        });


        thread.start();

        try {
            // 子线程插入到这里，直到运行结束，主线程才能继续
            thread.join();
            log.info("thread join over");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        log.info("num = {} ", num);

        log.info("main thread end");
    }

}
