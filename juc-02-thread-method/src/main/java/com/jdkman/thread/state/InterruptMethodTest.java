package com.jdkman.thread.state;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InterruptMethodTest {

    public static void main(String[] args) {
//        testSleepInterrupt();
        testRunnableInterrupt();
    }

    private static void testRunnableInterrupt() {
        Thread thread = new Thread(() -> {
            while(true) {
                if (Thread.currentThread().isInterrupted()) {
                    log.info("son thread is interrupted, exist");
                    return;
                }

                // sleep 才能相应中断？？？
//                SleepUtils.sleepWithoutExp(1000);

                log.info("hello, java");
            }
        });

        thread.start();

        SleepUtils.sleep(5000);

        thread.interrupt();

        SleepUtils.sleep(1000);

        log.info("main thread end.");
    }

    private static void testSleepInterrupt() {
        Thread t1 = new Thread(() -> {
            log.info("son thread begin");

            SleepUtils.sleepWithoutExp(5000);

            log.info("son thread end");
        });


        t1.start();

        // 终端子现场
        t1.interrupt();

        log.info("t1 state: {}", t1.getState());

        log.info("main thread end");
    }

}
