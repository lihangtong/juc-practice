package com.jdkman.thread.state;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SleepMethodTest {

    public static void main(String[] args) {
        test();
    }

    /**
     * sleep方法，会让当前线程从 RUNNABLE 状态 进入 TIMED_WAITING 状态
     * 其它线程可以用用interrupt方法，打断正在睡眠的线程；
     * 睡眠结束的线程未必会立刻得到执行，需要等待分配时间片；
     * 建议使用TimeUnit的sleep方法，代替Thread的sleep方法，来获得更好地可读性
     */
    private static void test() {

        Thread thread = new Thread(() -> {
            log.info("lihangtong thread start");

            Thread.State state = Thread.currentThread().getState();
            log.info("lihangtong thread state1 is : {}", state);

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            log.info("lihangtong thread end");
        });

        log.info("lihangtong thread state2 is : {}", thread.getState());
        thread.setName("lihangtong");
        thread.start();

        log.info("lihangtong thread state3 is : {}", thread.getState());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        log.info("lihangtong thread state4 is : {}", thread.getState());

    }


}
