package com.jdkman.thread.state;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试线程的优先级
 */
@Slf4j
public class PriorityMethodTest {

    public static void main(String[] args) {
        test();
    }

    /**
     * 只是给调度器的一个提示，决定权在调度器
     */
    private static void test() {
        Thread thread1 = new Thread(() -> {
            log.info("hello, java");
        });

        Thread thread2 = new Thread(() -> {
            log.info("hello, java");
        });

        Thread thread3 = new Thread(() -> {
            log.info("hello, java");
        });

        thread1.setPriority(1);
        thread2.setPriority(5);
        thread3.setPriority(10);


        thread1.start();
        thread2.start();
        thread3.start();

        log.info("main thread end");

    }

}
