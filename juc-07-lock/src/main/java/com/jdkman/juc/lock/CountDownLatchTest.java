package com.jdkman.juc.lock;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class CountDownLatchTest {

    private static CountDownLatch latch = new CountDownLatch(10);

    /**
     * 闭锁
     * 是一个同步工具类，通过一个计数器来实现。
     *
     * 应用场景：
     *  某个线程需要在其它n个线程执行完毕后，再向下执行。
     *
     *  方法：
     *      countDown()：每调用一次，计数器值就减去1
     *      await()：等待计数器变为0，即等待所有异步线程执行完毕。
     *      getCount()：获取剩余技数。
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        for (int i = 0; i < 10; i++) {
            int index = i;
            new Thread(() -> {
                log.info("thread begin");
                SleepUtils.sleep(1000 * index);
                latch.countDown();
                log.info("thread end, latch count:{}", latch.getCount());
            }).start();
        }

        try {
            log.info("latch await being");
            latch.await();
            log.info("latch await end.");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.in.read();
    }


}
