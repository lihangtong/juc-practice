package com.jdkman.juc.lock.locksupport;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.LockSupport;

@Slf4j
public class LockSupportTest {


    public static void main(String[] args) {

        log.info("main thread begin");

        log.info("begin to park.");
        LockSupport.park();

        log.info("main thread end");
    }


}
