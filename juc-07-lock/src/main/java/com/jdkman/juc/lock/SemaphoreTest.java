package com.jdkman.juc.lock;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.Semaphore;

@Slf4j
public class SemaphoreTest {

    private static Semaphore semaphore = new Semaphore(3);


    /**
     * 信号量
     * Semaphore 限制同时访问共享资源的线程数量
     *
     * 常用于 限流 等场景
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                try {
                    semaphore.acquire();
                    log.info("thread begin to acquire");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                SleepUtils.sleep(1000);

                semaphore.release();
                log.info("thread end to release.");
            }).start();
        }

        System.in.read();
    }


}
