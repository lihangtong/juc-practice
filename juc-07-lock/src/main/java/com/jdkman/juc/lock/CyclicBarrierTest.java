package com.jdkman.juc.lock;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

@Slf4j
public class CyclicBarrierTest {

    private static CyclicBarrier barrier = new CyclicBarrier(3);

    /**
     * 循环栅栏
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        for (int i = 0; i < 3; i++) {
            int index = i;
            new Thread(() -> {
                log.info("son thread begin");

                SleepUtils.sleep(1000 * index);

                try {
                    log.info("await other thread....");
                    barrier.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (BrokenBarrierException e) {
                    throw new RuntimeException(e);
                }
                log.info("son thread end.");
            }).start();
        }

        log.info("main thread end.");
        System.in.read();
    }


}
