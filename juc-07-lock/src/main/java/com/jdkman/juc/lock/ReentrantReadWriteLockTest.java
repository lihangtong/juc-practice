package com.jdkman.juc.lock;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReentrantReadWriteLockTest {

    /**
     * 读写互斥、写写互斥、读读不互斥
     * 读锁不支持条件变量
     * 持有读锁的情况下，去获取写锁，会被阻塞住。
     *
     * @param args
     */
    public static void main(String[] args) {
        DataContainer dataContainer = new DataContainer();
        new Thread(() -> {
            dataContainer.read();
        }, "thread-read-01").start();

        new Thread(() -> {
            dataContainer.read();
        }, "thread-read-02").start();

        new Thread(() -> {
            dataContainer.write("hello, java");
        }, "thread-write-01").start();

    }


}
