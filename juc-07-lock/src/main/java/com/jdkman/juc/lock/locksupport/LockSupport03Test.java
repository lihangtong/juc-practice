package com.jdkman.juc.lock.locksupport;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.LockSupport;

@Slf4j
public class LockSupport03Test {

    public static void main(String[] args) {

        log.info("main thread begin.");

        LockSupport.unpark(Thread.currentThread());

        SleepUtils.sleep(2000);

        // 已经有一个许可证了，所以此处就不会阻塞了
        LockSupport.park();

        log.info("main thread end.");


    }


}
