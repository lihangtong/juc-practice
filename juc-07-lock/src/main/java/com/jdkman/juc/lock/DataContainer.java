package com.jdkman.juc.lock;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 测试读写锁
 */
@Slf4j
public class DataContainer {

    private String data;

    private ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();

    private ReentrantReadWriteLock.ReadLock readLock = rwLock.readLock();

    private ReentrantReadWriteLock.WriteLock writeLock = rwLock.writeLock();


    public String read() {
        readLock.lock();
        String result;
        try {
            log.info("read begin");
            SleepUtils.sleep(1000);
            result = data;
            log.info("read finish.");
        } finally {
            readLock.unlock();
        }
        return result;
    }

    public void write(String data) {
        writeLock.lock();
        String result;
        try {
            log.info("write begin");
            this.data = data;
            log.info("write finish.");
        } finally {
            writeLock.unlock();
        }
    }


}
