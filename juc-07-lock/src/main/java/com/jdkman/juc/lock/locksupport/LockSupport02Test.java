package com.jdkman.juc.lock.locksupport;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.LockSupport;

@Slf4j
public class LockSupport02Test {


    public static void main(String[] args) {
        log.info("main thread begin");

        Thread thread = new Thread(() -> {
            log.info("begin to park.");
            LockSupport.park();
            log.info("begin to park end.");
        });

        thread.start();

        SleepUtils.sleep(2000);

        log.info("begin to unpark.");
        LockSupport.unpark(thread);
        log.info("main thread end");
    }


}
