package com.jdkman.juc.lock;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class ReentrantLockTest {

    private static ReentrantLock lock = new ReentrantLock();

    /**
     * 可重入锁
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                lock.lock();

                try {
                    log.info("get the lock");
                    SleepUtils.sleep(1000);
                    log.info("release the lock.");
                } finally {
                    lock.unlock();
                }
            }).start();
        }

        System.in.read();
    }


}
