package com.jdkman.juc.atomic;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;


@Slf4j
public class AtomicIntegerTest {

    /**
     * AtomicInteger
     * AtomicLong
     * AtomicBoolean
     */
    private static AtomicInteger inc = new AtomicInteger();

    public static void main(String[] args) {

        log.info("getAndIncrement = {}", inc.getAndIncrement());
        log.info("get = {}", inc.get());
        log.info("incrementAndGet = {}", inc.incrementAndGet());
        log.info("addAndGet = {}", inc.addAndGet(3));
    }


}
