package com.jdkman.thread.create.thread;

public class ThreadTest {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> System.out.println("hello,java"));
        thread.start();
        System.out.println("thread = " + thread.getName());
    }


}
