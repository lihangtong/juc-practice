package com.jdkman.thread.create.callable;

import java.util.concurrent.Callable;

public class CallableTask implements Callable<String> {

    @Override
    public String call() throws Exception {
        String name = Thread.currentThread().getName();
        System.out.println("name = " + name);
        return "hello, java";
    }

}
