package com.jdkman.thread.create.callable;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

@Slf4j
public class ThreadCallTest {

    public static void main(String[] args) {
        // 用futureTask 包一下 Callable实现，可以获取返回值
        FutureTask<String> futureTask = new FutureTask<>(new CallableTask());

        Thread thread = new Thread(futureTask);

        thread.start();

        String s = null;
        try {
            // get() 获取结果方法，会抛出两个异常，中断异常和执行异常
            s = futureTask.get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

        log.info("s = {}", s);

        log.info("main thread = " + thread.getName());
    }

}
