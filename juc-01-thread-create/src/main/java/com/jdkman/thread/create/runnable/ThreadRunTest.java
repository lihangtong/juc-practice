package com.jdkman.thread.create.runnable;

public class ThreadRunTest {


    public static void main(String[] args) {
        runnable01();

        System.out.println("------------------------------------");

        runnable02();
    }

    private static void runnable02() {
        Thread thread = new Thread(() -> System.out.println("hello, java juc"));
        thread.start();

        System.out.println("runnable02 end");
    }

    private static void runnable01() {
        Thread thread = new Thread(new RunnableTask());

        thread.start();

        String name = Thread.currentThread().getName();
        System.out.println("runnable01 end");
    }


}
