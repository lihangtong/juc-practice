package com.jdkman.thread.pool;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 */
@Slf4j
public class CachedThreadPoolTest {

    public static void main(String[] args) {
        // 看下方法的实现，采用了同步队列，线程数量Integer.MAX_VALUE
        ExecutorService threadPool = Executors.newCachedThreadPool();

        threadPool.submit(() -> {
           log.info("hello, java");
        });
    }


}
