package com.jdkman.thread.pool;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class ShutdownNowMethodTest {

    public static void main(String[] args) {
        ExecutorService threadPool = Executors.newFixedThreadPool(2);

        threadPool.execute(() -> {
            log.info("thread 1 begin");
            SleepUtils.sleep(1000);
            log.info("thread 1 end");
        });

        threadPool.execute(() -> {
            log.info("thread 2 begin");
            SleepUtils.sleep(1000);
            log.info("thread 2 end");
        });

        threadPool.execute(() -> {
            log.info("thread 3 begin");
            SleepUtils.sleep(1000);
            log.info("thread 3 end");
        });

        // 不会接受新任务
        // 会将队列中的任务返回
        // 并用interrupt的方式，中断正在执行的任务
        threadPool.shutdownNow();

    }

}
