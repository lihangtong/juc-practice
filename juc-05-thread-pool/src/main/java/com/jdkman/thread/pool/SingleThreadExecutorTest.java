package com.jdkman.thread.pool;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class SingleThreadExecutorTest {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.submit(() -> {
           log.info("thread 1 start");
            SleepUtils.sleep(1000);
           log.info("thread 1 end");
        });

        executorService.submit(() -> {
            log.info("thread 2 start");
            SleepUtils.sleep(1000);
            log.info("thread 2 end");
        });

        executorService.submit(() -> {
            log.info("thread 3 start");
            SleepUtils.sleep(1000);
            log.info("thread 3 end");
        });


        // 只是打印日志，未关闭线程池
        log.info("main thread end.");

    }


}
