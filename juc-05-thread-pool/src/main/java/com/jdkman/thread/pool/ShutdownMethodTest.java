package com.jdkman.thread.pool;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ShutdownMethodTest {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        executorService.execute(() -> {
            log.info("thread 1 begin");
            SleepUtils.sleep(1000);
            log.info("thread 1 end");
        });

        executorService.execute(() -> {
            log.info("thread 2 begin");
            SleepUtils.sleep(2000);
            log.info("thread 2 end");
        });

        executorService.execute(() -> {
            log.info("thread 3 begin");
            SleepUtils.sleep(3000);
            log.info("thread 3 end");
        });


        // 不会接受新任务
        // 已提交任务会执行完
        executorService.shutdown();

        try {
            // 等待队列总的任务执行完成，最多5s
            executorService.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("main thread end");
    }

}
