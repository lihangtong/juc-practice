package com.jdkman.thread.pool;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class FixedThreadPoolTest {

    public static void main(String[] args) {
        ExecutorService threadPool = Executors.newFixedThreadPool(4);

        threadPool.submit(FixedThreadPoolTest::toRun);
        threadPool.submit(FixedThreadPoolTest::toRun);
        threadPool.submit(FixedThreadPoolTest::toRun);
        threadPool.submit(FixedThreadPoolTest::toRun);
        threadPool.submit(FixedThreadPoolTest::toRun);
        threadPool.submit(FixedThreadPoolTest::toRun);
        threadPool.submit(FixedThreadPoolTest::toRun);
        threadPool.submit(FixedThreadPoolTest::toRun);

        log.info("main thread exit.but thread pool not terminate.");

        // 关闭线程池，任务会继续处理，处理完成后，会推出
//        threadPool.shutdown();
        // 关闭线程池，任务会舍弃
        threadPool.shutdownNow();
    }

    private static void toRun() {
        log.info("thread begin");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        log.info("thread end");
    }


}
