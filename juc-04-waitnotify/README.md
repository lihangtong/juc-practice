
### Park& Unpark
他们是LockSupport中的方法
```java
    // 暂停当前线程
    LockSupport.park();

    // 恢复某个线程的运行
    LockSupport.unpark(被暂停的线程对象);
```
### 特点
与Objet的wait & notify 相比
1. wait，notify和notifyAll必须配合 Object monitor一起使用，而 park，unpark 不必；
2. park & unpark 是以线程为单位来 【阻塞】 和 【唤醒】 线程，而 notify 只能随机唤醒一个等待线程，notifyAll 是唤醒所有等待线程，就不那么精确。
3. park & unpark 可以先unpark，而 wait & notify 不能先notify.

### 原理
每个线程都有自己的一个Parker对象，由三部分组成 _counter, _cond 和 _mutex。









