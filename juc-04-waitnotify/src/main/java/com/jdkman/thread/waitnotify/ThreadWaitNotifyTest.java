package com.jdkman.thread.waitnotify;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadWaitNotifyTest {

    public static void main(String[] args) {
        NumberPrinter printer = new NumberPrinter();

        new Thread(() -> {
            while (true) {
                printer.printOs();
            }
        }).start();

        new Thread(() -> {
            while (true) {
                printer.printJs();
            }
        }).start();

    }

}
