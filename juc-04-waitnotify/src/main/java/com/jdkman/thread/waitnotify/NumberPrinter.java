package com.jdkman.thread.waitnotify;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class NumberPrinter{

    private Object mointor = new Object();

    private int i;

    public void printJs() {
        synchronized (mointor) {
            while (i % 2 == 0) {
                try {
                    mointor.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            log.info("{}", i);
            i++;
            SleepUtils.sleep(2000);
            mointor.notifyAll();
        }

    }

    public void printOs() {
        synchronized (mointor) {
            while (i % 2 == 1) {
                try {
                    mointor.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            log.info("{}", i);
            i++;
            SleepUtils.sleep(2000);
            mointor.notifyAll();
        }

    }


}
