package com.jdkman.thread.waitnotify;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
public class ThreadWaitNotify2Test {

    private static BlockingQueue<String> queue = new LinkedBlockingQueue<>();

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            int index = i;
            new Thread(() -> {
                try {
                    SleepUtils.sleep(1000);
                    queue.put("热菜" + index);
                    log.info("生产 : {}", "热菜" + index);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }).start();
        }

        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                while (true) {
                    try {
                        String take = queue.take();
                        log.info("take :{}", take);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }).start();
        }



    }


}
