package com.jdkman.thread.waitnotify;

import com.jdkman.common.SleepUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

@Slf4j
public class LockSupportTest {

    public static void main(String[] args) {
        test();
    }

    private static void test() {
        Thread t1 = new Thread(() -> {
            log.info("son thread start");
            SleepUtils.sleep(1000);

            log.info("LockSupport park");
            LockSupport.park();

            log.info("son thread end");
        });

        t1.start();


        SleepUtils.sleep(2000);
        log.info("LockSupport unpark");

        // unpark 可以在 park钱进行调用，后续park依然可以被唤醒
        LockSupport.unpark(t1);

        log.info("main thread end.");

    }


}
