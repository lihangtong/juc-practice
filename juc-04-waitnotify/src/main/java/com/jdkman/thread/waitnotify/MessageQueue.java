package com.jdkman.thread.waitnotify;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class MessageQueue<T>{

    private List<T> list = new ArrayList<>();

    private int capacity;

    public MessageQueue(int capacity) {
        this.capacity = capacity;
    }


    public T take() {
        synchronized (list) {
            while (list.isEmpty()) {
                try {
                    list.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            T remove = list.remove(0);
            log.info("take :{}", remove);
            // 唤醒生成者线程
            list.notifyAll();
            return remove;
        }
    }

    public void put(T t) {
        synchronized (list) {
            while (list.size() >= capacity) {
                try {
                    list.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            list.add(t);
            log.info("put :{}", t);
            // 唤醒消费者去消费
            list.notifyAll();
        }
    }


}
